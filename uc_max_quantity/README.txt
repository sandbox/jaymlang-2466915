INTRODUCTION
------------
The Ubercart User Maximum feature is designed to create a new product feature called 'User Maximum'
that will limit users to ordering a specified amount per rolling calendar year.
E.g. setting a 'User Maximum' of 50 on a product would limit users to ordering a maximum of 50
products over the past 12 months.

* For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/JayMLang/2466915

* To submit bug reports or feature suggestions:
  https://www.drupal.org/project/issues/2466915


REQUIREMENTS
------------
* Ubercart (https://www.drupal.org/project/ubercart)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
* Customizing a product feature can be done under the 'Features' tab of the Node Edit page for an Ubercart product.


MAINTAINERS
-----------
Current Maintainers:
 * Justin Langley - https://www.drupal.org/u/justin-langley
